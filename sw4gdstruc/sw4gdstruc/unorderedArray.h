#pragma once
#include <assert.h>

template<class T>

class unorderedArray
{
public:
	unorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0); 

		}

	}

	virtual ~unorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL); 
		if (mNumElements >= mMaxSize)
		{
			expand();
		}
		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}
	int getSize() 
	{
		return mNumElements;
	}



	/*virtual void pop()
	{
		if (mNullElements > 0)
		{
			mNullElements--;
		}
	}*/



	virtual void remove(int index)
	{
		assert(mArray != NULL);
		if (index >= mMaxSize) // if the index is greater that current size
		{
			return;
		}
		for (int i = index; i < mMaxSize - 1; i++)
		{
			mArray[i] = mArray[i + 1];
			
		}
		mMaxSize--;

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize;
		}

	}


	virtual search(int index, int v)
	{
		assert(mArray != NULL);
		
		for (int i = 0; i < mMaxSize; i++)
		{
			if (index == v)
			{
				return index;
			}
		}
		return -1;
	}

private:
	T* mArray; // mArray is a generic Type
	int mMaxSize; //m means private variable
	int mRowSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}
		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);
		memcpy(temp, mArray, sizeof(T) * mMaxSize);
		delete[] mArray;
		mArray = temp;
		mMaxSize += mGrowSize;
		return true;
	}
};
