#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
#include "unorderedArray.h"
using namespace std;

void main()
{
	srand(time(NULL));
	unorderedArray<int> grades(5);

	int size = 0;
	cout << "Size? ";
	cin >> size;

	for (int i = 1; i <= size; i++)// randomize vector
	{
		int c = rand() % 101;
		grades.push(c); //resizeable vector
	}

	cout << "Before delete" << endl;
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	int erase;
	cout << "What element to remove?";
	cin >> erase;

	
	grades.remove(erase);

	

	cout << "After delete" << endl;
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}
	
	cout << "Find Element: ";
	int input;
	cin >> input;
	int found;

	grades.search(input, size);
	if (found != -1)
	{
		cout << "Element found at " << found << endl;
	}
	else
	{
		cout << "Element not found." << endl;
	}



	system("pause");
}
