#include<iostream>
#include<string>
#include <time.h>
using namespace std;

//1
void reverseString(string &input, int i)
{
	if (i == input.length()/2)
	{
		return;
	}
	reverseString(input, i + 1);
    swap(input[i], input[input.size() - i - 1]);
}

//2 ** 
bool findTriplets(int array[], int num, int sum, int triplet)
{	
	if (triplet == 3 || sum == 0)
	{
		cout << "Triplets found." << endl;
		return true;
	}
	else if (triplet == 3 || num == 0 || sum < 0)
	{
		cout << "Triplets not found." << endl;
		return false;
	}
	return findTriplets(array, num - 1, sum - array[num - 1], triplet + 1) || findTriplets(array, num - 1, sum, triplet);
}

//3
int binarySearch(int bArray[], int x, int last, int first)
{
	if (first >= last)
	{
		int center = (last + first) / 2;
		if (bArray[center] == x)
		{
			return center;
		}
		if (bArray[center] > x)
		{
			return binarySearch(bArray, x, last, center - 1);
		}
		return binarySearch(bArray, x, center + 1, first);
	}
	return -1;
}

int main()
{
	srand(time(NULL));
	//1
	int i = 0;
	cout << "1. Reverse String" << endl;
	cout << "Input String: ";
	string input;
	getline(cin, input);
	cout << input << endl;
	cout << "Reversed: ";
	reverseString(input, 0);
	cout << input << endl;
	system("pause");
	system("cls");

	//2 ** 
	int sum = 0, triplet = 0;
	cout << "2. Triplets" << endl;
	int array[10];
	int arraySize = sizeof(array) / sizeof(array[0]);
	for (int i = 0; i < arraySize; i++)
	{
		array[i] = rand() % 10 + 1;
	}
	for (int i = 0; i < arraySize; i++)
	{
		cout << array[i] << " ";

	}
	findTriplets(array, arraySize, sum, triplet);
	system("pause");
	system("cls");

	//3
	cout << "3. Binary Search" << endl;
	int bArray[10];
	int barraySize = sizeof(bArray) / sizeof(bArray[0]);
	for (int i = 0; i < barraySize; i++)
	{
		bArray[i] = rand() % 10 + 1;
	}
	for (int i = 0; i < barraySize; i++)
	{
		cout << bArray[i] << " ";
		
	}
	cout << endl;
	cout << "Input number: ";
	int y;
	cin >> y;
	cout << endl;
	int result = binarySearch(bArray, y, 0, barraySize - 1);
	if (result == -1)
	{
		cout << "Not found." << endl;
	}
	else 
	{
		cout << "Found. Took " << result + 1 << " steps to find key." << endl;
	}

	system("pause");
	return 0;
}