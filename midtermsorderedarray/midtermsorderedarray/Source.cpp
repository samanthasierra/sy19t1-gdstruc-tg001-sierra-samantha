#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
	}

	for (int i = 0; i < size; i++)
	{
		int random = rand() % 101;
		ordered.push(random);
	}
	
	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";

	cout << "\n\nEnter number to search: ";
	int input;
	cin >> input;
	cout << endl;

	cout << "Unordered Array(Linear Search):\n";
	int result = unordered.linearSearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;

	cout << "Ordered Array(Binary Search):\n";
	result = ordered.binarySearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;

	int text;
	cout << "Expand? (1 = yes; 2 = no;)" << endl;
	cin >> text;
	if (text == 1)
	{
		for (int i = 0; i < size; i++)
		{
			int rng = rand() % 101;
			unordered.push(rng);
		}

		for (int i = 0; i < size; i++)
		{
			int random = rand() % 101;
			ordered.push(random);
		}
		cout << "\nGenerated array: " << endl;

		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
	}
	else if (text == 2)
	{
		system("pause");
	}

	system("pause");
}