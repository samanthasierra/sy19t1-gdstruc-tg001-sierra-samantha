#pragma once
#include <assert.h>
#include <iostream>
#include <vector>
#include <string>

template<class T>
class Stack
{
public:
	Stack(int size, int growBy = 1) : mArrayS(NULL), mMaxSizeS(0), mGrowSizeS(0), mNumElementsS(-1)
	{
		if (size)
		{
			mMaxSizeS = size;
			mArrayS = new T[mMaxSizeS];
			memset(mArrayS, 0, sizeof(T) * mMaxSizeS);
			mGrowSizeS = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~Stack()
	{
		if (mArrayS != NULL)
		{
			delete[] mArrayS;
			mArrayS = NULL;
		}
	}

	//push element
	virtual void push(T value)
	{
		assert(mArrayS != NULL);
		mArrayS[++mNumElementsS] = value;
	}

	// top element
	const T& top()
	{
		return mArrayS[mNumElementsS];
	}

	//  remove element
	virtual void pop()
	{
		if (mNumElementsS >= 0)
			mNumElementsS--;

		if (mNumElementsS <= -1)
		{
			mNumElementsS = -1;
		}
	}

	virtual T& operator[](int index)
	{
		assert(mArrayS != NULL && index <= mNumElementsS);
		return mArrayS[index];
	}

	virtual int getSize()
	{
		return mNumElementsS;
	}

private:
	T* mArrayS;
	int mGrowSizeS, mMaxSizeS, mNumElementsS;
};