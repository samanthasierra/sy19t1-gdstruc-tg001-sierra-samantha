#pragma once
#include <assert.h>
#include <iostream>
#include <vector>
#include <string>

template<class T>
class Queue
{
public:
	Queue(int size, int growBy = 1) : mArrayQ(NULL), mMaxSizeQ(0), mGrowSizeQ(0), mNumElementsQ(0)
	{
		if (size)
		{
			mMaxSizeQ = size;
			mArrayQ = new T[mMaxSizeQ];
			memset(mArrayQ, 0, sizeof(T) * mMaxSizeQ);
			mGrowSizeQ = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~Queue()
	{
		if (mArrayQ != NULL)
		{
			delete[] mArrayQ;
			mArrayQ = NULL;
		}
	}

	//push element
	virtual void push(T value)
	{
		assert(mArrayQ != NULL);
		mArrayQ[mNumElementsQ] = value;
		mNumElementsQ++;
	}

	//front element
	const T& front()
	{
		return mArrayQ[0];
	}

	//top element
	const T& top()
	{
		return mArrayQ[mNumElementsQ];
	}

	//remove element
	virtual void pop()
	{
		for (int i = 0; i < mMaxSizeQ; i++)
		{
			mArrayQ[i] = mArrayQ[i + 1];
		}
		mNumElementsQ--;

		if (mNumElementsQ <= 0)
		{
			mNumElementsQ = 0;
		}
	}

	virtual T& operator[](int index)
	{
		assert(mArrayQ != NULL && index <= mNumElementsQ);
		return mArrayQ[index];
	}

	virtual int getSize()
	{
		return mNumElementsQ;
	}

private:
	T* mArrayQ;
	int mGrowSizeQ, mMaxSizeQ, mNumElementsQ;
};