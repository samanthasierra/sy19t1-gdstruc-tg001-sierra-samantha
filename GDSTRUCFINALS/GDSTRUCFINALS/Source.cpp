#include <iostream>
#include <string>
#include <vector>
#include "Queue.h"
#include "Stack.h"

using namespace std;

int main()
{
	cout << "Enter size for element sets: ";
	int size;
	cin >> size;
	cout << endl;
	Queue<int> queue(size);
	Stack<int> stack(size);

	int choice;

	do
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Push Elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		cout << "4 - Exit" << endl;

		cin >> choice;
		cout << endl;

		switch (choice)
		{
		case 1: //push element
			int number;
			cout << "Enter number: ";
			cin >> number;
			cout << endl;

			queue.push(number);
			stack.push(number);

			cout << "Top element of Sets: ";
			cout << endl;
			cout << "Queue elements: " << queue.front() << endl;
			cout << "Stack elements: " << stack.top() << endl;
			cout << endl;

			system("pause");
			system("cls");
			break;

		case 2: //pop element
			queue.pop();
			stack.pop();
			
			cout << "You have popped the front elements." << endl;

			system("pause");
			system("cls");
			break;

		case 3: // print all + delete all
			//print
			cout << "Queue elements: " << endl;
			for (int i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}

			cout << "Stack elements: " << endl;
			for (int i = stack.getSize(); i >= 0; i--)
			{
				cout << stack[i] << endl;
			}

			//delete
			for (int i = queue.getSize() + 1; i >= -1; i--)
			{
				queue.pop();
			}

			for (int i = stack.getSize() - 1; i >= -1; i--)
			{
				stack.pop();
			}

			system("pause");
			system("cls");
			break;

		case 4:
			//exit
			cout << "Exit" << endl;
			break;

		default:
			//invalid choice
			cout << "Choice " << choice << " invalid." << endl;

			system("pause");
			system("cls");
		}
	} while (choice != 4);

	system("pause");
	return 0;
}