#include<iostream>
#include<string>
using namespace std;

//1
int addNum(int num)
{
	
	if (num <= 0)
	{
		return 0;
	}
	
	return (num % 10 + addNum(num / 10));
}

//2
int fibonacci(int num)
{

	if (num == 0)
	{
		return 0;
	}
	else if (num == 1)
	{
		return 1;
	}
	
	return fibonacci(num - 1) + fibonacci(num - 2);

}

//3
int ifPrime(int num, int i)
{
	if (i == 1)
	{
		return 1;
	}

	else if (num < 2)
	{
		return 0;
	}
	
	else if (num % i == 0)
	{
		return 0;
	}

	return ifPrime(num, i - 1);
}


int main()
{
	//1
	int num1;
	cout << "Number: ";
	cin >> num1;
	int result= addNum(num1);
	cout << "Sum: " << result << endl;

	//2
	int num2;
	cout << "Number: ";
	cin >> num2;
	for (int i = 1; i <= num2; i++)
	{
		cout << fibonacci(i) << " ";
	}
	cout << endl;

	//3
	int num3, result3;
	cout << "Number: ";
	cin >> num3;
	result3 = ifPrime(num3, num3 / 2);
	if (result3 == 0)
	{
		cout << "Not prime number." << endl;
	}
	else
	{
		cout << "Prime number." << endl;
	}

	system("pause");
	return 0;
}